

const os = require('os') // Core Module atau module utama dari Node.js
const segitiga = require('./segitiga')
const fs = require('fs')

console.log("Memory Free:", os.freemem())

console.log(segitiga.luasSegita(20,40))
console.log(segitiga.luasPersegi(20))

const isiFile = fs.readFileSync('./nama.txt', 'utf-8')

fs.writeFileSync('tes.txt', 'Saya sedang belajar Node js')

console.log(isiFile)


// Install nodemon:
// Buka terminal di Vs Code
// npm install -g nodemon [enter]
// Untuk runningnya:
// nodemon [enter]

// Kalo error jalankan perintah ini:
// Open powershell as an administrator
// Lalu jalankan function ini
// Set-ExecutionPolicy RemoteSigned -Scope CurrentUser